import unittest
from main import a0, checked, v0, v1, v2, v3


class TestStringMethods(unittest.TestCase):

    # Unit Test (Antwort in Datenbank richtig gesetzt?)
    def test_q1answer1(self):
        self.assertEqual(a0[0], "Neptun")

    # Integration Test (volle Punktzahl)
    def test_fullendscore(self):
        expected_result = 4
        v0.set(1)
        v1.set(2)
        v2.set(0)
        v3.set(1)
        actual_result = checked()
        self.assertEqual(expected_result, actual_result)

    # Integration Test (2 Punkte erhalten)
    def test_2endscore(self):
        expected_result = 2
        v0.set(0)
        v1.set(2)
        v2.set(3)
        v3.set(1)
        actual_result = checked()
        self.assertEqual(expected_result, actual_result)


if __name__ == "__main__":
    unittest.main()
